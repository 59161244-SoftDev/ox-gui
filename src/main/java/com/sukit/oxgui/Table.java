/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.oxgui;

import java.io.Serializable;

/**
 *
 * @author Xenon
 */
public class Table implements Serializable{

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean isFinish = false;
    private int lastCol;
    private int lastRow;
    private int counter;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
        counter = 0;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    public char getRowCol(int row, int col) {
        return table[row][col];
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            //remember last row col input
            this.lastRow = row;
            this.lastCol = col;
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void turnCycle() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }
    
    private void setWinLoseStat() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }
    
    public void increaseCounter() {
        this.counter++;
    }
    
    public int getCounter() {
        return counter;
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;
        setWinLoseStat();
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        isFinish = true;
        winner = currentPlayer;
        setWinLoseStat();
    }
    
    void checkDiag() {
        if (table[1][1] == currentPlayer.getName()) {
            if (table[0][0] == currentPlayer.getName()
                    && table[2][2] == currentPlayer.getName()) {
                isFinish = true;
                winner = currentPlayer;
                setWinLoseStat();
            } else if (table[0][2] == currentPlayer.getName() 
                    && table[2][0] == currentPlayer.getName()) {
                isFinish = true;
                winner = currentPlayer;
                setWinLoseStat();
            }
        }
    }

    public boolean checkDraw() {
        if(counter > 8){
            isFinish = true;
            winner = null;
            playerO.draw();
            playerX.draw();
            return true;
        }
        return false;
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkDiag();
        if(counter > 8 && !isFinish){
            isFinish = true;
            winner = null;
            playerO.draw();
            playerX.draw();
        }
    }

    public boolean checkFinish() {
        return isFinish;
    }

    public Player getWinner() {
        return winner;
    }
}
